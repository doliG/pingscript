declare function require(string: any): any;
declare var process;
const sendmail = require('sendmail');
import { IMailerConfig } from "config";

export class Mailer {
  config: IMailerConfig;

  constructor(conf: IMailerConfig) {
    this.config = conf;
    try {
      this.checkTemplates();
    } catch (e) {
      process.exitCode = 1;
    }
  }

  checkTemplates(): void {
    this.getFilledTemplate(this.config.subjectTemplate);

    if (this.config.textTemplate) {
      this.getFilledTemplate(this.config.textTemplate);
    }
    if (this.config.htmlTemplate) {
      this.getFilledTemplate(this.config.htmlTemplate);
    }
  }

  getFilledTemplate(s: string): string {
    const selector = /{{.+?}}/g;
    const evaluate = (s) => {
      const { host } = this.config;
      const expression = s.substring(2, s.length - 2).trim();
      let evaluted = '';

      try {
        evaluted = eval(expression);
      } catch(e) {
        throw new Error('There is an error in your template. Cannot eval `' + expression + '`');
      }
      return evaluted;
    }

    return s.replace(selector, evaluate);
  }

  sendMail(): void {
    const { from, to, htmlTemplate: html, textTemplate: text } = this.config;
    console.log("Sending mail to: " + to.toString());
    // console.log("Object: " + this.getFilledTemplate(this.config.subjectTemplate));

    // console.log("Body: `" + this.getFilledTemplate(this.config.textTemplate) + '`');

    sendmail({
      from,
      to: to.toString(),
      subject: 'Awesome Subject',
      html: html ? html : null,
      text: text ? text : null,
    }, function(err, reply) {
      console.log(err && err.stack);
      console.dir(reply);
    });
  }
}
