export interface IConfig {
  host: string;
  intervalTime: number;
  retryTime: number;
  mailerConfig: IMailerConfig;
  serverRebootConfig?: IServerRebootConfig;
}

export interface IMailerConfig {
  host: string;
  from?: string;
  to: Array<string>;
  subjectTemplate?: string;
  htmlTemplate?: string;
  textTemplate?: string;
}

/**
 * OVH ONLY
 * You need to create keys  and store them in you
 * env (explanations TODO)
 */
export interface IServerRebootConfig {
  /**
   * For example vps123456.ovh.net
   */
  vpsName: string,

  /**
   *  In ms. The time your server will need to reboot.
   *  The script will wait this time before ping it again.
  */
  estimatedRebootTime: number;
}
