import { IConfig } from "./types/config";
import { Pinger } from "./pinger";
import { Mailer } from "./mailer";
import { ServerReboot } from "./server-reboot";

/**
 * Disponible dans les templates :
 * - js pur (expression évaluée par node)
 * - host (l'addresse du serveur/nom de domaine)
 */
const subjectTemplate = `Server {{ host }} down at {{ new Date().toLocaleString() }}`;
const textTemplate = `Hello,
Your server {{ host }} as been unreachable at {{ new Date().toLocaleString() }}
Please reboot it then reboot this script too.
Kiss`;

const configs: Array<IConfig> = [
  {
    host: 'prod2.kawaa.co',
    retryTime: 6000,
    intervalTime: 5000,
    mailerConfig: {
      host: 'prod2.kawaa.co',
      to: ['guillaume.lodi@kawaa.co'],
      textTemplate,
      subjectTemplate,
    },
    serverRebootConfig: {
      vpsName: 'vps546833.ovh.net', // prod2.kawaa.co
      estimatedRebootTime: 120000
    },
  },
];

configs.forEach(c => new Pinger(c));
