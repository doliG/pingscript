declare function require(string: any): any;
declare var process;
const ovh = require('ovh');

import { IServerRebootConfig } from "config";

export class ServerReboot {
  config: IServerRebootConfig;
  ovh: any; // OVH wrapper

  constructor(config: IServerRebootConfig) {
    this.config = config;
    const ovhEnv = {
      appKey: process.env.OVH_APP_KEY,
      appSecret: process.env.OVH_APP_SECRET,
      consumerKey: process.env.OVH_CONSUMER_KEY,
    }
    console.log(ovhEnv);
    this.ovh = ovh(ovhEnv);
  }

  reboot(): void {
    const c = this.config;

    // console.log('[FAKE] Call OVH api to reboot ', c.vpsName);

    // this.ovh.request('GET', `/vps/${c.vpsName}`, function (err, res) {
    //   if (err) {
    //     console.error(`ERR /vps/${c.vpsName}: `, JSON.stringify(err, null, 2));
    //   } else {
    //     console.log(`RES /vps/${c.vpsName}: `, res.displayName);
    //   }
    // });

    // this.ovh.request('GET', '/me', function(err, res) {
    //   console.log('err=', err);
    //   console.log('res=', res);
    // });

    // Todo: throw and catch err here
    this.ovh.request('POST', `/vps/${c.vpsName}/reboot`, function (err, res) {
      if (err) {
        console.log(`ERR /vps/${c.vpsName}/reboot: ` + err);
      } else {
        console.log(`SCS /vps/${c.vpsName}/reboot`);
      }
    });
  }
}
