declare function require(string: any): any;
const domainPing = require('domain-ping');

import { IConfig } from './types/config';
import { Mailer } from './mailer';
import { ServerReboot } from './server-reboot';

type Tstatus = 'isAlive'
  | 'hasFailedOnce'
  | 'hasRebootedServer'
  | 'hasFailedTwice'
  | 'hasSentErrorMail';

export class Pinger {
  config: IConfig;
  intervalId: number;
  status: Tstatus = 'isAlive';

  mailer: Mailer;
  serverReboot: ServerReboot;

  constructor(conf: IConfig) {
    this.mailer = new Mailer(conf.mailerConfig);
    if (conf.serverRebootConfig) {
      this.serverReboot = new ServerReboot(conf.serverRebootConfig);
    }
    this.config = conf;
    this.setPingInterval(this.config.intervalTime);
    console.log('Pinger.name = ' + conf.host);
  }

  removePingInterval(): void {
    clearInterval(this.intervalId);
    this.intervalId = null;
  }

  setPingInterval(length: number): void {
    if (this.intervalId) {
      throw new Error('[Error] IntervalID is already defined');
    }

    console.log(`setInterval for ${this.config.host} of ${length / 1000}s`);
    this.intervalId = setInterval(() => {
      domainPing(this.config.host)
        .then(res => this.handleSuccess(res))
        .catch(err => this.handleError(err));
    }, length);
  }

  handleSuccess(res): void {
    console.log(`[ALIVE] ${new Date().toLocaleString()} ${this.config.host}`);
    switch (this.status) {
      case 'hasRebootedServer': {
        clearInterval(this.intervalId);
        this.status = 'isAlive';
        this.setPingInterval(this.config.intervalTime);
      }
    }
  }

  handleError(err): void {
    console.log(`[DEAD] ${new Date().toLocaleString()} ${this.config.host} oldStatus='${this.status}'`);
    this.removePingInterval();
    switch (this.status) {
      case 'isAlive': {
        this.status = 'hasFailedOnce';
        this.setPingInterval(this.config.retryTime);
        break;
      }

      case 'hasFailedOnce': {
        if (this.serverReboot) {
          this.serverReboot.reboot();
          this.setPingInterval(this.config.serverRebootConfig.estimatedRebootTime);
          this.status = 'hasRebootedServer';
        } else {
          this.mailer.sendMail();
          this.status = 'hasSentErrorMail';
        }
        break;
      }

      case 'hasRebootedServer': {
        this.mailer.sendMail();
        this.status = 'hasSentErrorMail';
        break;
      }
    }
  }
}
